extends Area2D

# IGNORE
func _process(delta):
	if Input.is_mouse_button_pressed(BUTTON_LEFT):
		if _is_mouse_in_area():
			#print("Maus wurde beim Objekt gedrückt: ", self.name)
			pass

# IGNORE
func _is_mouse_in_area():
	var mouse_position = $CollisionShape2D.get_local_mouse_position()
	if mouse_position.x >= -32 and mouse_position.x <= 32 and mouse_position.y >= -32 and mouse_position.y <= 32:
		return true
	return false

# CLICK
func click():
	print("Turret '", self.name , "' was clicked!")


func _on_input_event(viewport, event, shape_idx):
    if event is InputEventMouseButton \
    and event.button_index == BUTTON_LEFT \
    and event.is_pressed():
        self.click()
