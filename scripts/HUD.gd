extends CanvasLayer

# Blur Animation
var current_blur_value = 0
var target_blur_value = 1.7
var blur_fade_speed = 1.5
var blur_fade_in = false
var blur_fade_out = false

func _ready():
	$GameOver.visible = false
	set_blur_value(0)
	pass

func show_game_over():
	$GameOver.visible = true
	$GameOver/GameOverText/Animation.play("GameOverText_FadeIn")
	add_blur()

func _process(delta):
	if blur_fade_in:
		current_blur_value += 1 * blur_fade_speed * delta
		if current_blur_value > target_blur_value:
			current_blur_value = target_blur_value
			blur_fade_in = false
			print("FADED IN")
			#remove_blur()
	elif blur_fade_out:
		current_blur_value -= 1 * blur_fade_speed * delta
		if current_blur_value <= 0:
			current_blur_value = 0
			blur_fade_out = false
			$BlurEffect.visible = false
			print("FADED OUT")
	set_blur_value(current_blur_value)

func add_blur():
	blur_fade_out = false
	
	set_blur_value(0)
	$BlurEffect.visible = true
	blur_fade_in = true

func remove_blur():
	blur_fade_in = false
	
	set_blur_value(target_blur_value)
	blur_fade_out = true

func set_blur_value(value):
	$BlurEffect.get_material().set_shader_param("blur", value)