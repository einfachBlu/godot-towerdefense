extends Node2D

export (PackedScene) var Enemy
var enemy_count = 0
var health = 100
var healthBarAnimationSpeed = 40

func _ready():
	$HUD/HealthBar.value = health
	$MobSpawnTimer.start()
	pass

func _process(delta):
	if $HUD/HealthBar.value > health:
		if $HUD/HealthBar.value - (delta * healthBarAnimationSpeed) < health:
			$HUD/HealthBar.value = health
			if health == 0:
				game_over()
			return
		
		$HUD/HealthBar.value -= (delta * healthBarAnimationSpeed)

func _on_MobSpawnTimer_timeout():
	if enemy_count >= 5:
		return
	var enemy = Enemy.instance()
	var pathfollow2d_node = PathFollow2D.new()
	$"Path2D".add_child(pathfollow2d_node)
	
	pathfollow2d_node.add_child(enemy)
	enemy.follow_node = pathfollow2d_node
	enemy_count += 1

func game_over():
	# Remove all Enemies
	for i in range(0, $"Path2D".get_child_count()):
		$"Path2D".get_child(i).queue_free()
	
	# Stop SpawnTimer
	$MobSpawnTimer.stop()
	
	# Show GameOver HUD
	$HUD.show_game_over()
	

func _on_EndArea_area_entered(area):
	health -= 10
	if health < 0:
		health = 0
